let gallery = (document.querySelector('.gallery-with-filter')) ? document.querySelector('.gallery-with-filter') : null;

if (gallery !== null) {
    let galleryFilterList = (Array.from(gallery.querySelectorAll('input:not(#allImages)'))) ? Array.from(gallery.querySelectorAll('input:not(#allImages)')) : null;

    console.log("fds")
    if (galleryFilterList.length !== 0) {
        let imgContainer = gallery.querySelector("div");
        let galleryFilterList = Array.from(gallery.querySelectorAll('input'));
        let galleryImgList = Array.from(gallery.getElementsByTagName('img'));
        let galleryTextList = Array.from(gallery.getElementsByTagName('h3'));
        let filterChecked = galleryFilterList.filter(input => input.checked);
        let classImgs = galleryImgList.filter(img => img.classList.contains(filterChecked[0].id));

        const columnCount = () => {
            let windowWidth = window.innerWidth;
            if (windowWidth <= 1024) {
                if (classImgs.length < 3) {
                    imgContainer.style.columnCount = classImgs.length.toString();
                } else {
                    imgContainer.style.columnCount = "3";
                }
            }
            if (windowWidth <= 768) {
                if (classImgs.length < 2) {
                    imgContainer.style.columnCount = classImgs.length.toString();
                } else {
                    imgContainer.style.columnCount = "2";
                }
            }
            if (windowWidth <= 545) {
                imgContainer.style.columnCount = "1";
            }
            if (windowWidth >= 1025) {
                if (classImgs.length < 4) {
                    imgContainer.style.columnCount = classImgs.length.toString();
                } else {
                    imgContainer.style.columnCount = '4';
                }
            }
        }

        for (let i of galleryFilterList) {
            i.addEventListener('click', function () {
                classImgs = galleryImgList.filter(img => img.classList.contains(i.id));
                let classText = galleryTextList.filter(text => text.classList.contains(i.id));

                columnCount();

                galleryImgList.forEach(img => img.style.display = "none");
                classImgs.forEach(img => img.style.display = "inline");

                galleryTextList.forEach(text => text.style.display = "none");
                classText.forEach(text => text.style.display = "inline");
            })
        }

        window.addEventListener('resize', columnCount);
    } else {
        let allImageBtn = gallery.querySelector("label[for='allImages']") ? gallery.querySelector("label[for='allImages']") : null;
        if (allImageBtn !== null) allImageBtn.style.display = "none";
    }
}