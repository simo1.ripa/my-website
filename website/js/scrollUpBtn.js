const ScrollToTopBtn = document.querySelector(".scroll-up-btn")

document.addEventListener("scroll", () => {
    if (window.pageYOffset > 200 ) {
        ScrollToTopBtn.classList.add("show-btn")
        ScrollToTopBtn.disabled = false;
    } else {
        ScrollToTopBtn.classList.remove("show-btn")
        ScrollToTopBtn.disabled = true;
    }
})

const goToTop = () => {
    document.body.scrollIntoView({
        behavior: "smooth",
    });
};

ScrollToTopBtn.addEventListener("click", goToTop)