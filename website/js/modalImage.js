let modal = document.getElementById('modalContainer');
let images = document.getElementsByClassName('clickable-img');
let modalImg = document.getElementById("modalImg");
let modalText = document.getElementById("modalText");

for (let i = 0; i < images.length; i++) {
    let img = images[i];
    img.onclick = function () {
        modal.style.display = "block";
        modalImg.src = this.src;
        modalImg.alt = this.alt;
        modalText.innerHTML = this.alt;
    }
}

let closeModalBtn = document.getElementById("closeModalBtn");
closeModalBtn.onclick = function () {
    modal.style.display = "none";
}